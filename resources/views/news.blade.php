@extends('master.subpage')

@section('containers')

	@include('sections.sub-banner')

	<section id="news-container">
		<div class="col-md-12"> 
			<div class="linebreak"></div>
			<h2 class="title text-center">News</h2>
			<p class="subtitle text-center">Here you will find our latest news, information and promotions</p>

			<div class="media">
			  	<div class="media-left">
				    <i class="fa fa-file-text"></i>
			  	</div>
			  	<div class="media-body">
			    	<h4 class="media-heading">News heading</h4>
			   		<p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with 'real' content. This is required when, for example, the final text is not yet available. Dummy text is also known as 'fill text'. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a 'ready-made' text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>

			   		<p class="text-right">
			   			<a href="" class="btn btn-primary btn-sm">Read More</a>
			   		</p>
			  	</div>
			</div>
			<div class="media">
			  	<div class="media-left">
				    <i class="fa fa-file-text"></i>
			  	</div>
			  	<div class="media-body">
			    	<h4 class="media-heading">News heading</h4>
			   		<p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with 'real' content. This is required when, for example, the final text is not yet available. Dummy text is also known as 'fill text'. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a 'ready-made' text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>

			   		<p class="text-right">
			   			<a href="" class="btn btn-primary btn-sm">Read More</a>
			   		</p>
			  	</div>
			</div>
			<div class="media">
			  	<div class="media-left">
				    <i class="fa fa-file-text"></i>
			  	</div>
			  	<div class="media-body">
			    	<h4 class="media-heading">News heading</h4>
			   		<p>Dummy text is text that is used in the publishing industry or by web designers to occupy the space which will later be filled with 'real' content. This is required when, for example, the final text is not yet available. Dummy text is also known as 'fill text'. It is said that song composers of the past used dummy texts as lyrics when writing melodies in order to have a 'ready-made' text to sing with the melody. Dummy texts have been in use by typesetters since the 16th century.</p>

			   		<p class="text-right">
			   			<a href="" class="btn btn-primary btn-sm">Read More</a>
			   		</p>
			  	</div>
			</div>
			
		</div>
		<div class="clearfix"></div>
	</section>

@stop