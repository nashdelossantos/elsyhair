@extends('master.base')

@section('containers')
	
	@include('sections.full-banner')

	@include('sections.gallery')

	@include('sections.latest-products')

	@include('sections.opening-hours')

@stop